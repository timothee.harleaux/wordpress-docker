FROM wordpress:latest

ENV COMPOSER_VERSION 2.1.8
ENV XDEBUG_VERSION 3.1.5


RUN curl -sS https://getcomposer.org/installer \
    | php -- --filename=composer --install-dir=/usr/local/bin --version=${COMPOSER_VERSION} \
    COPY --from=composer /usr/bin/composer /usr/bin/composer
# install lastest version of xdebug and enable it
RUN pecl install xdebug-${XDEBUG_VERSION} \
    && docker-php-ext-enable xdebug

